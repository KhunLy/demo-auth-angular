import { Component, OnInit } from '@angular/core';
import { ProductModel } from 'src/app/models/productModel';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  model: ProductModel[];

  constructor(
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.productService.getAll()
      .subscribe(
        data => this.model = data,
        error => console.log
      );
  }

}
