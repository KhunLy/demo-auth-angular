import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  fg: FormGroup;
  constructor(
    private authService: AuthService,
    private session: SessionService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(2)
      ])),
    }) 
  }

  login() {
    if(this.fg.invalid) return;
    this.authService.login(this.fg.value)
      .subscribe(
        data => {
          this.session.start(data);
          this.router.navigateByUrl('/home');
        },
        error => {
          console.log(error);
        }
      );

  }

}
