import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductModel } from '../models/productModel';
import { environment } from 'src/environments/environment';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private httpClient: HttpClient,
    private session: SessionService
  ) { }


  public getAll(offset= 0, limit = 100, keyword = null): Observable<ProductModel[]> {
    return this.httpClient.get<ProductModel[]>(
      environment.apiDomain + 'product?' + 'limit=' + limit + '&offset=' + offset
    );
  }
}
