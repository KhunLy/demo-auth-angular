import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  items= [
    {title: 'Home', link: '/home'},
    {title: 'Product', link: '/product'},
  ]
}
